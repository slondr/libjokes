/* Copyright 2019-2020 Eric S. Londres
This program is free software. you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


fn render_joke(res: String) -> String {
    if let Ok(json::JsonValue::Object(o)) = json::parse(res.as_str()) {
	if let Some(t) = o["type"].as_str() {
	    match t {
		"single" => format!("{}", o["joke"]),
		"twopart" => format!("{}\n{}", o["setup"], o["delivery"]),
		_ => "".to_string()
	    }
	} else {
	    panic!("Could not find type")
	}
    } else {
	panic!("Could not parse JSON")
    }
}

pub fn get_joke() -> String {
    let res = reqwest::blocking::get("https://sv443.net/jokeapi/v2/joke/Any");
    match res {
	Ok(r) => render_joke(r.text().unwrap()),
	Err(e) => panic!("{}", e.to_string())
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        crate::get_joke();
    }
}
